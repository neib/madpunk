#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame
from game import Game


SCREEN_WIDTH = 800
SCREEN_HEIGHT = 576

running = True
pygame.init()

while running:

    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("PACMAN")

    clock = pygame.time.Clock()
    game = Game()


    done = False
    while not done:
        done = game.process_events()
        game.run_logic()
        game.display_frame(screen)
        # 30 fps/s
        clock.tick(30)

    pygame.quit()

