#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame
from MadPunk import Player
from Enemies import *
from Map import *


SCREEN_WIDTH = 800
SCREEN_HEIGHT = 576

BLACK = (0,0,0)
WHITE = (255,255,255)
BLUE = (0,0,255)
GREEN = (0,255,0)
RED = (255,0,0)

global win
win = False

class Pills(pygame.sprite.Sprite):
    def __init__(self,x,y,change_x,change_y):
        pygame.sprite.Sprite.__init__(self)
        self.change_x = change_x
        self.change_y = change_y
        self.image = pygame.image.load("Textures/pill.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)

class Way(pygame.sprite.Sprite):
    def __init__(self,x,y,change_x,change_y):
        pygame.sprite.Sprite.__init__(self)
        self.change_x = change_x
        self.change_y = change_y
        self.image = pygame.image.load("Textures/alpha.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)

class Game(object):
    def __init__(self, bestScore = 0, MadPunk_score=0, MadClown_score=0):
        self.font = pygame.font.Font(None,40)
        self.about = False
        self.highscores = False
        self.game_over = True
        self.win = False


        self.MadPunk_score = MadPunk_score
        self.MadClown_score = MadClown_score
        self.score = MadPunk_score + MadClown_score
        self.bestScore = bestScore

        print(self.game_over)
        if self.score != 0 and self.score % 205 == 0:
            self.game_over = False
            self.win = True
            print(self.game_over)

        self.font = pygame.font.Font(None,35)

        #Menu
        if self.game_over and not self.win:
            self.menu = Menu(("New Game","High Scores","About","Exit"),font_color = WHITE, font_size = 60)

        #Player
        self.player = Player(34,130,"Textures/MadPunk.png")
        self.madclown = MadClown(162,322,2,0)

        #Créer les blocs pour le chemin.
        self.horizontal_blocks = pygame.sprite.Group()
        self.vertical_blocks = pygame.sprite.Group()

        self.topleft_blocks = pygame.sprite.Group()
        self.topright_blocks = pygame.sprite.Group()
        self.bottomleft_blocks = pygame.sprite.Group()
        self.bottomright_blocks = pygame.sprite.Group()

        self.lefttopright_blocks = pygame.sprite.Group()
        self.leftbottomright_blocks = pygame.sprite.Group()
        self.topleftbottom_blocks = pygame.sprite.Group()
        self.toprightbottom_blocks = pygame.sprite.Group()

        self.left_blocks = pygame.sprite.Group()
        self.right_blocks = pygame.sprite.Group()
        self.top_blocks = pygame.sprite.Group()
        self.bottom_blocks = pygame.sprite.Group()

        self.start_block = pygame.sprite.Group()

        self.dots_group = pygame.sprite.Group()


        for i, row in enumerate(environment()):
            for j, item in enumerate(row):
                if item == 11:
                    self.horizontal_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 12:
                    self.vertical_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 21:
                    self.topleft_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 22:
                    self.topright_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 23:
                    self.bottomleft_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 24:
                    self.bottomright_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 31:
                    self.lefttopright_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 32:
                    self.leftbottomright_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 33:
                    self.topleftbottom_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 34:
                    self.toprightbottom_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 41:
                    self.top_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 42:
                    self.bottom_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 43:
                    self.left_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 44:
                    self.right_blocks.add(Way(j*32+8, i*32+8, 16, 16))
                elif item == 54:
                    self.start_block.add(Way(j*32+8, i*32+8, 16, 16))

        #Création des ennemis.
        self.enemies = pygame.sprite.Group()
        self.enemies.add(Cat(160,66,5,0))
        self.enemies.add(Nurse(290,96,0,2))
        self.enemies.add(Nurse(290,320,0,-2))
        self.enemies.add(Nurse(546,64,0,2))
        self.enemies.add(Nurse(34,228,0,2))
        self.enemies.add(Nurse(160,66,2,0))
        self.enemies.add(Nurse(448,66,-2,0))
        self.enemies.add(Nurse(640,322,2,0))
        self.enemies.add(Psy(640,450,1,0))

        #Ajoute les pills sur le terrain.
        for i, row in enumerate(environment()):
            for j, item in enumerate(row):
                if item != 10 and item != 54:
                    self.dots_group.add(Pills(j*32+7, i*32+7, 8, 8))

        #Charge les effets sonores
        self.MadPunk_sound = pygame.mixer.Sound("Sounds/springfling.wav")
        self.MadClown_sound = pygame.mixer.Sound("Sounds/choose.wav")


    def process_events(self):
        for event in pygame.event.get(): #Détection des actions du joueur.
            if event.type == pygame.QUIT:
                return True

            self.menu.event_handler(event)

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    if self.game_over and not self.about:
                        if self.menu.state == 0:

                            # ---- START ---- #
                            if self.MadPunk_score > self.bestScore:
                                self.bestScore = self.MadPunk_score
                            self.__init__(self.bestScore)
                            self.game_over = False
                            pygame.mixer.music.stop()
                            inGameMusic = pygame.mixer.music.load("Musics/fanatic/2.ogg")
                            pygame.mixer.music.play(-1)

                        elif self.menu.state == 1:

                            # - HIGHSCORES -- #
                            self.highscores = True

                        elif self.menu.state == 2:

                            # ---- ABOUT ---- #
                            self.about = True

                        elif self.menu.state == 3:

                            # ---- QUIT ---- #
                            return True

                elif event.key == pygame.K_RIGHT:
                    self.player.move_right()
                elif event.key == pygame.K_LEFT:
                    self.player.move_left()
                elif event.key == pygame.K_UP:
                    self.player.move_up()
                elif event.key == pygame.K_DOWN:
                    self.player.move_down()

                elif event.key == pygame.K_ESCAPE:
                    if self.game_over and not self.about and not self.highscores:
                        return True

                    self.game_over = True
                    self.about = False
                    self.highscores = False
                    if not self.menu.state == 1:
                        pygame.mixer.music.stop()
                        menuMusic = pygame.mixer.music.load("Musics/fanatic/4.ogg")
                        pygame.mixer.music.play(-1)
                    self.menu.state = 0


            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    self.player.stop_move_right()
                elif event.key == pygame.K_LEFT:
                    self.player.stop_move_left()
                elif event.key == pygame.K_UP:
                    self.player.stop_move_up()
                elif event.key == pygame.K_DOWN:
                    self.player.stop_move_down()

            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.player.explosion = True
                self.menu.state = 0

        return False


    def PC_Collide(self):
        if self.player.rect.left in range(self.madclown.rect.left, self.madclown.rect.right) and self.player.rect.top in range(self.madclown.rect.top, self.madclown.rect.bottom):
            return True
        elif self.player.rect.left in range(self.madclown.rect.left, self.madclown.rect.right) and self.player.rect.bottom in range(self.madclown.rect.top, self.madclown.rect.bottom):
            return True
        elif self.player.rect.right in range(self.madclown.rect.left, self.madclown.rect.right) and self.player.rect.top in range(self.madclown.rect.top, self.madclown.rect.bottom):
            return True
        elif self.player.rect.right in range(self.madclown.rect.left, self.madclown.rect.right) and self.player.rect.bottom in range(self.madclown.rect.top, self.madclown.rect.bottom):
            return True
        else:
            return False


    def run_logic(self):
        global bestScore

        if not self.game_over:

            self.player.update(self.horizontal_blocks, self.vertical_blocks,
                 self.topleft_blocks, self.topright_blocks, self.bottomleft_blocks, self.bottomright_blocks,
                 self.lefttopright_blocks, self.leftbottomright_blocks, self.topleftbottom_blocks, self.toprightbottom_blocks,
                 self.left_blocks, self.right_blocks, self.top_blocks, self.bottom_blocks, self.start_block)

            block_hit_list = pygame.sprite.spritecollide(self.player, self.dots_group, True)
            if len(block_hit_list) > 0:
                self.MadPunk_sound.play()
                self.MadPunk_score += 1

            block_hit_list = pygame.sprite.spritecollide(self.player, self.enemies, True)
            if len(block_hit_list) > 0:
                self.player.explosion = True
                self.menu.state = 0

            self.game_over = self.player.game_over
            self.enemies.update(self.horizontal_blocks, self.vertical_blocks)

            #######
            self.madclown.update(self.horizontal_blocks, self.vertical_blocks)
            block_hit_list = pygame.sprite.spritecollide(self.madclown, self.dots_group, True)
            if len(block_hit_list) > 0:
                self.MadClown_sound.play()
                self.MadClown_score += 1


            if (self.MadPunk_score + self.MadClown_score) % 205 == 0:
                    pygame.time.wait(500)
                    self.__init__(self.bestScore, self.MadPunk_score, self.MadClown_score)

             #######
            Clown_Collide = self.PC_Collide()
            if Clown_Collide:
                self.player.explosion = True
                self.menu.state = 0




    def display_frame(self, screen):
        screen.fill(BLACK)
        mainTitleBackground = pygame.image.load("Textures/MainTitle.png")
        screen.blit(mainTitleBackground, (0,0))

        if self.game_over:
            if self.about:
                text = self.font.render("Music by Marc A. Pullen (aka Fanatic)", True, RED)
                screen.blit(text, [20, 115])
                text = self.font.render("<marcpullen at comcast dot net>.", True, RED)
                screen.blit(text, [150, 140])
            elif self.highscores:
                text = self.font.render("HIGH SCORES", True, RED)
                screen.blit(text, [270, 115])

                n = 150
                with open('HighScores', 'r') as file:
                    content = file.read()
                    scores = content.split('\n')
                    for line in scores:
                        text = self.font.render(line, True, RED)
                        screen.blit(text, [290, n])
                        n += 25
            else:
                self.menu.display_frame(screen)
        else:
            # Dessine le terrain.
            bg = pygame.image.load("Textures/Background.png")
            screen.blit(bg, (0,0))

            self.horizontal_blocks.draw(screen)
            self.vertical_blocks.draw(screen)
            draw_environment(screen)
            self.dots_group.draw(screen)
            self.enemies.draw(screen)
            screen.blit(self.player.image, self.player.rect)
            screen.blit(self.madclown.image, self.madclown.rect)
            # Affiche le score
            text = self.font.render("Score : " + (str)(self.MadPunk_score), True, GREEN)
            screen.blit(text, [80, 25])
            text = self.font.render("Best score : " + (str)(self.bestScore), True, BLUE)
            screen.blit(text, [345, 25])
            text = self.font.render("MadClown : " + (str)(self.MadClown_score), True, BLUE)
            screen.blit(text, [575, 25])

        # Update.
        pygame.display.flip()


    def display_message(self, screen, message, color=RED):
        label = self.font.render(message, True, color)
        width = label.get_width()
        height = label.get_height()
        posX = (SCREEN_WIDTH/2) - (width/2)
        posY = (SCREEN_HEIGHT/2) - (height/2)
        screen.blit(label,(posX,posY))




class Menu(object):
    state = 0
    def __init__(self, items, font_color=BLACK, select_color=RED, ttf_font=None, font_size=25):
        self.font_color = font_color
        self.select_color = select_color
        self.items = items
        self.font = pygame.font.Font(ttf_font, font_size)
        menuMusic = pygame.mixer.music.load("Musics/fanatic/4.ogg")
        pygame.mixer.music.play(-1)

    def display_frame(self, screen):
        for index, items in enumerate(self.items):
            if self.state == index:
                label = self.font.render(items, True, self.select_color)
            else:
                label = self.font.render(items, True, self.font_color)

            width = label.get_width()
            height = label.get_height()

            posX = (SCREEN_WIDTH/2)-(width/2)
            t_h = len(self.items)*height
            posY = (SCREEN_HEIGHT/2)-(t_h/2)+(index*height)

            screen.blit(label,(posX,posY))

    def event_handler(self,event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if self.state > 0:
                    self.state -= 1
            elif event.key == pygame.K_DOWN:
                if self.state < len(self.items) -1:
                    self.state += 1


