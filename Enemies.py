#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame
import random
from MadPunk import Animation
from Map import *


SCREEN_WIDTH = 800
SCREEN_HEIGHT = 576
BLACK = (0,0,0)
WHITE = (255,255,255)
BLUE = (0,0,255)
GREEN = (0,255,0)
RED = (255,0,0)


class Block(pygame.sprite.Sprite):
    def __init__(self,x,y,color,width,height):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([width,height])
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)


class Cat(pygame.sprite.Sprite):
    def __init__(self,x,y,change_x,change_y):
        pygame.sprite.Sprite.__init__(self)
        self.change_x = change_x
        self.image = pygame.image.load("Textures/cat.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)


    def update(self,horizontal_blocks,vertical_blocks):
        self.rect.x += self.change_x
        if self.rect.right < 0:
            self.rect.left = SCREEN_WIDTH
        elif self.rect.left > SCREEN_WIDTH:
            self.rect.right = 0


class MadClown(pygame.sprite.Sprite):
    change_x = 0
    change_y = 0

    def __init__(self,x,y,change_x,change_y):
        pygame.sprite.Sprite.__init__(self)
        self.change_x = change_x
        self.change_y = change_y
        self.image = pygame.image.load("Textures/MadClown.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)


        img = pygame.image.load("Textures/MadClown.png").convert()
        self.move_right_animation = Animation(self.image,32,32)
        self.move_left_animation = Animation(pygame.transform.flip(img, True, False), 32, 32)
        self.move_up_animation = Animation(self.image,32,32)
        self.move_down_animation = Animation(pygame.transform.flip(img, True, False), 32, 32)


    def update(self,horizontal_blocks,vertical_blocks):
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        if self.rect.right < 0:
            self.rect.left = SCREEN_WIDTH
        elif self.rect.left > SCREEN_WIDTH:
            self.rect.right = 0
        if self.rect.bottom < 0:
            self.rect.top = SCREEN_HEIGHT
        elif self.rect.top > SCREEN_HEIGHT:
            self.rect.bottom = 0

        self.center = (self.rect.centerx, self.rect.centery)
        direction = choose_direction(self.center)


        if direction == "left" and self.change_x == 0:
            self.change_x = -2
            self.change_y = 0
            self.move_left_animation.update(10)
            self.image = self.move_left_animation.get_current_image()

        elif direction == "right" and self.change_x == 0:
            self.change_x = 2
            self.change_y = 0
            self.move_right_animation.update(10)
            self.image = self.move_right_animation.get_current_image()

        elif direction == "up" and self.change_y == 0:
            self.change_x = 0
            self.change_y = -2
            self.move_up_animation.update(10)
            self.image = self.move_up_animation.get_current_image()

        elif direction == "down" and self.change_y == 0:
            self.change_x = 0
            self.change_y = 2
            self.move_down_animation.update(10)
            self.image = self.move_down_animation.get_current_image()


        elif direction == "left" and self.change_y == 0:
            self.change_x = -2
            self.change_y = 0
            self.move_left_animation.update(10)
            self.image = self.move_left_animation.get_current_image()

        elif direction == "right" and self.change_y == 0:
            self.change_x = 2
            self.change_y = 0
            self.move_right_animation.update(10)
            self.image = self.move_right_animation.get_current_image()

        elif direction == "up" and self.change_x == 0:
            self.change_x = 0
            self.change_y = -2
            self.move_up_animation.update(10)
            self.image = self.move_up_animation.get_current_image()

        elif direction == "down" and self.change_x == 0:
            self.change_x = 0
            self.change_y = 2
            self.move_down_animation.update(10)
            self.image = self.move_down_animation.get_current_image()






class Psy(pygame.sprite.Sprite):
    def __init__(self,x,y,change_x,change_y):
        pygame.sprite.Sprite.__init__(self)
        self.change_x = change_x
        self.change_y = change_y

        self.image = pygame.image.load("Textures/Psy.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)



        img = pygame.image.load("Textures/Psy.png").convert()
        self.move_right_animation = Animation(self.image,32,32)
        self.move_left_animation = Animation(img,32,32)
        self.move_up_animation = Animation(self.image,32,32)
        self.move_down_animation = Animation(img,32,32)

    def update(self,horizontal_blocks,vertical_blocks):
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        if self.rect.right < 0:
            self.rect.left = SCREEN_WIDTH
        elif self.rect.left > SCREEN_WIDTH:
            self.rect.right = 0
        if self.rect.bottom < 0:
            self.rect.top = SCREEN_HEIGHT
        elif self.rect.top > SCREEN_HEIGHT:
            self.rect.bottom = 0

        self.center = (self.rect.centerx, self.rect.centery)
        direction = choose_direction(self.center)


        if direction == "left" and self.change_x == 0:
            self.change_x = -1
            self.change_y = 0
            self.move_left_animation.update(10)
            self.image = self.move_left_animation.get_current_image()

        elif direction == "right" and self.change_x == 0:
            self.change_x = 1
            self.change_y = 0
            self.move_right_animation.update(10)
            self.image = self.move_right_animation.get_current_image()

        elif direction == "up" and self.change_y == 0:
            self.change_x = 0
            self.change_y = -1
            self.move_up_animation.update(10)
            self.image = self.move_up_animation.get_current_image()

        elif direction == "down" and self.change_y == 0:
            self.change_x = 0
            self.change_y = 1
            self.move_down_animation.update(10)
            self.image = self.move_down_animation.get_current_image()


        elif direction == "left" and self.change_y == 0:
            self.change_x = -1
            self.change_y = 0
            self.move_left_animation.update(10)
            self.image = self.move_left_animation.get_current_image()

        elif direction == "right" and self.change_y == 0:
            self.change_x = 1
            self.change_y = 0
            self.move_right_animation.update(10)
            self.image = self.move_right_animation.get_current_image()

        elif direction == "up" and self.change_x == 0:
            self.change_x = 0
            self.change_y = -1
            self.move_up_animation.update(10)
            self.image = self.move_up_animation.get_current_image()

        elif direction == "down" and self.change_x == 0:
            self.change_x = 0
            self.change_y = 1
            self.move_down_animation.update(10)
            self.image = self.move_down_animation.get_current_image()





class Madboy(pygame.sprite.Sprite):
    def __init__(self,x,y,change_x,change_y):
        pygame.sprite.Sprite.__init__(self)
        self.change_x = change_x
        self.change_y = change_y
        self.image = pygame.image.load("Textures/madboy.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)

    def update(self,horizontal_blocks,vertical_blocks):
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        if self.rect.right < 0:
            self.rect.left = SCREEN_WIDTH
        elif self.rect.left > SCREEN_WIDTH:
            self.rect.right = 0
        if self.rect.bottom < 0:
            self.rect.top = SCREEN_HEIGHT
        elif self.rect.top > SCREEN_HEIGHT:
            self.rect.bottom = 0

        if self.rect.topleft in self.get_intersection_position():
            direction = random.choice(("left","right","up","down"))
            if direction == "left" and self.change_x == 0:
                self.change_x = -3
                self.change_y = 0
            elif direction == "right" and self.change_x == 0:
                self.change_x = 3
                self.change_y = 0
            elif direction == "up" and self.change_y == 0:
                self.change_x = 0
                self.change_y = -3
            elif direction == "down" and self.change_x == 0:
                self.change_x = 0
                self.change_y = 3

    def get_intersection_position(self):
        items = []
        for i, row in enumerate(environment()):
            for j, item in enumerate(row):
                if item == 13:
                    items.append((j*32,i*32))
        return items



class Nurse(pygame.sprite.Sprite):
    def __init__(self,x,y,change_x,change_y):
        pygame.sprite.Sprite.__init__(self)
        self.change_x = change_x
        self.change_y = change_y

        self.image = pygame.image.load("Textures/nurse.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)

        self.move_right_animation = Animation(self.image,32,32)
        self.move_left_animation = Animation(self.image,32,32)
        self.move_up_animation = Animation(self.image,32,32)
        self.move_down_animation = Animation(self.image,32,32)



    def update(self,horizontal_blocks,vertical_blocks):
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        if self.rect.right < 0:
            self.rect.left = SCREEN_WIDTH
        elif self.rect.left > SCREEN_WIDTH:
            self.rect.right = 0
        if self.rect.bottom < 0:
            self.rect.top = SCREEN_HEIGHT
        elif self.rect.top > SCREEN_HEIGHT:
            self.rect.bottom = 0

        self.center = (self.rect.centerx, self.rect.centery)
        direction = choose_direction(self.center)


        if direction == "left" and self.change_x == 0:
            self.change_x = -2
            self.change_y = 0
            self.move_left_animation.update(10)
            self.image = self.move_left_animation.get_current_image()

        elif direction == "right" and self.change_x == 0:
            self.change_x = 2
            self.change_y = 0
            self.move_right_animation.update(10)
            self.image = self.move_right_animation.get_current_image()

        elif direction == "up" and self.change_y == 0:
            self.change_x = 0
            self.change_y = -2
            self.move_up_animation.update(10)
            self.image = self.move_up_animation.get_current_image()

        elif direction == "down" and self.change_y == 0:
            self.change_x = 0
            self.change_y = 2
            self.move_down_animation.update(10)
            self.image = self.move_down_animation.get_current_image()


        elif direction == "left" and self.change_y == 0:
            self.change_x = -2
            self.change_y = 0
            self.move_left_animation.update(10)
            self.image = self.move_left_animation.get_current_image()

        elif direction == "right" and self.change_y == 0:
            self.change_x = 2
            self.change_y = 0
            self.move_right_animation.update(10)
            self.image = self.move_right_animation.get_current_image()

        elif direction == "up" and self.change_x == 0:
            self.change_x = 0
            self.change_y = -2
            self.move_up_animation.update(10)
            self.image = self.move_up_animation.get_current_image()

        elif direction == "down" and self.change_x == 0:
            self.change_x = 0
            self.change_y = 2
            self.move_down_animation.update(10)
            self.image = self.move_down_animation.get_current_image()


