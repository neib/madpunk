#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame
import random


BLACK = (0,0,0)
BLUE = (0,10,255)



def environment():
    grid = ((10,12,10,10,10,10,10,41,10,12,10,10,10,10,10,10,10,12,10,10,10,10,10,12,10),
            (10,12,10,10,10,10,10,10,10,12,10,10,10,10,10,10,10,12,10,10,10,10,10,12,10),
            (11,13,11,11,11,11,11,11,11,13,11,11,32,11,11,11,11,31,11,11,11,11,11,31,11),
            (10,12,10,10,10,10,10,10,10,12,10,10,12,10,10,10,10,10,10,10,10,10,10,10,10),
            (10,54,11,11,11,11,11,11,11,33,10,10,22,11,11,23,10,10,10,10,10,10,10,42,10),
            (10,12,10,10,10,10,10,10,10,12,10,10,10,10,10,12,10,10,10,10,10,10,10,12,10),
            (11,13,11,11,11,32,11,11,11,33,10,24,11,11,11,13,11,32,11,11,11,23,10,34,11),
            (10,12,10,10,10,12,10,10,10,12,10,12,10,10,10,12,10,12,10,10,10,22,11,33,10),
            (10,12,10,10,10,12,10,10,10,34,11,21,10,44,11,21,10,12,10,10,10,10,10,12,10),
            (10,12,10,10,10,12,10,10,10,12,10,10,10,10,10,10,10,12,10,10,10,10,10,12,10),
            (11,13,11,32,11,31,32,11,11,13,11,11,11,11,11,32,11,13,11,11,11,11,11,13,11),
            (10,12,10,12,10,10,12,10,10,41,10,10,10,10,10,41,10,12,10,10,10,10,10,12,10),
            (10,12,10,34,11,11,21,10,10,10,10,10,10,10,10,10,10,34,11,11,11,11,11,33,10),
            (10,12,10,12,10,10,10,10,10,42,10,10,10,10,10,10,10,12,10,10,10,10,10,12,10),
            (11,13,11,31,11,11,11,11,11,13,11,43,10,10,24,11,11,13,11,11,32,11,11,31,11),
            (10,12,10,10,10,10,10,10,10,12,10,10,10,10,12,10,10,12,10,10,12,10,10,10,10),
            (10,12,10,44,32,23,10,10,10,12,10,10,10,10,12,10,10,34,11,32,21,10,10,42,10),
            (10,12,10,10,22,31,11,32,11,13,11,11,43,10,22,11,11,33,10,41,10,10,10,12,10))

    return grid


def draw_environment(screen):
    for i, row in enumerate(environment()):
            for j, item in enumerate(row):
                # ---------------------------------------------- #
                #   HORIZONTAL AND VERTICAL LINES                #
                # ---------------------------------------------- #

                if item == 11: # Horizontal Lines
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32+32, i*32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32+32], [j*32+32, i*32+32], 3)

                elif item == 12: # Vertical Lines
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)
                    pygame.draw.line(screen, BLACK,[j*32+32, i*32], [j*32+32, i*32+32], 3) #X2

                # ---------------------------------------------- #
                #   TURNS                                        #
                # ---------------------------------------------- #

                elif item == 21: # TopLeft
                    pygame.draw.line(screen, BLACK,[j*32, i*32+32], [j*32+32, i*32+32], 3)
                    pygame.draw.line(screen, BLACK,[j*32+32, i*32], [j*32+32, i*32+32], 3) #X2
                elif item == 22: # TopRight # OK
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32+32], [j*32+32, i*32+32], 3)

                elif item == 23: # BottomLeft # OK
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32+32, i*32], 3)
                    pygame.draw.line(screen, BLACK,[j*32+32, i*32], [j*32+32, i*32+32], 3) #X2
                elif item == 24: # BottomRight
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32+32, i*32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)

                # ---------------------------------------------- #
                #   3 WAYS TURNS                                 #
                # ---------------------------------------------- #

                elif item == 31: # LTR
                    pygame.draw.line(screen, BLACK,[j*32, i*32+32], [j*32+32, i*32+32], 3)
                elif item == 32: # LBR
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32+32, i*32], 3)

                elif item == 33: # TLB
                    pygame.draw.line(screen, BLACK,[j*32+32, i*32], [j*32+32, i*32+32], 3) #X2
                elif item == 34: # TRB
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)

                # ---------------------------------------------- #
                #   ONE WAY                                      #
                # ---------------------------------------------- #

                elif item == 41: # Top
                    pygame.draw.line(screen, BLACK,[j*32, i*32+32], [j*32+32, i*32+32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)
                    pygame.draw.line(screen, BLACK,[j*32+32, i*32], [j*32+32, i*32+32], 3) #X2
                elif item == 42: # Bottom
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32+32, i*32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)
                    pygame.draw.line(screen, BLACK,[j*32+32, i*32], [j*32+32, i*32+32], 3) #X2

                elif item == 43: # Left
                    pygame.draw.line(screen, BLACK,[j*32+32, i*32], [j*32+32, i*32+32], 3) #X2
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32+32, i*32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32+32], [j*32+32, i*32+32], 3)
                elif item == 44: # Right
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32+32, i*32], 3)
                    pygame.draw.line(screen, BLACK,[j*32, i*32+32], [j*32+32, i*32+32], 3)


                # ---------------------------------------------- #
                #   START POSITION                               #
                # ---------------------------------------------- #
                elif item == 54:
                    pygame.draw.line(screen, BLACK,[j*32, i*32], [j*32, i*32+32], 3)



def my_way(rect_center):

    if rect_center in get_intersection_position():
        direction = ("left","right","up","down")
        return direction

    elif rect_center in get_lefttop_position():
        direction = ("left","up")
        return direction
    elif rect_center in get_leftbottom_position():
        direction = ("left","down")
        return direction
    elif rect_center in get_righttop_position():
        direction = ("right","up")
        return direction
    elif rect_center in get_rightbottom_position():
        direction = ("right","down")
        return direction

    elif rect_center in get_lefttopright_position():
        direction = ("left","right","up")
        return direction
    elif rect_center in get_leftbottomright_position():
        direction = ("left","right","down")
        return direction
    elif rect_center in get_topleftbottom_position():
        direction = ("left","up","down")
        return direction
    elif rect_center in get_toprightbottom_position():
        direction = ("right","up","down")
        return direction

    elif rect_center in get_left_position():
        direction = "left"
        return direction
    elif rect_center in get_right_position():
        direction = "right"
        return direction
    elif rect_center in get_top_position():
        direction = "up"
        return direction
    elif rect_center in get_bottom_position():
        direction = "down"
        return direction


    elif rect_center in get_horizontal_position():
        direction = ("left","right")
        return direction

    elif rect_center in get_vertical_position():
        direction = ("up","down")
        return direction

    else:
        return "noway"





def choose_direction(rect_center):

    if rect_center in get_intersection_position():
        direction = random.choice(("left","right","up","down"))
        return direction

    elif rect_center in get_lefttop_position():
        direction = random.choice(("left","up"))
        return direction
    elif rect_center in get_leftbottom_position():
        direction = random.choice(("left","down"))
        return direction
    elif rect_center in get_righttop_position():
        direction = random.choice(("right","up"))
        return direction
    elif rect_center in get_rightbottom_position():
        direction = random.choice(("right","down"))
        return direction

    elif rect_center in get_lefttopright_position():
        direction = random.choice(("left","right","up"))
        return direction
    elif rect_center in get_leftbottomright_position():
        direction = random.choice(("left","right","down"))
        return direction
    elif rect_center in get_topleftbottom_position():
        direction = random.choice(("left","up","down"))
        return direction
    elif rect_center in get_toprightbottom_position():
        direction = random.choice(("right","up","down"))
        return direction

    elif rect_center in get_left_position():
        direction = "left"
        return direction
    elif rect_center in get_right_position():
        direction = "right"
        return direction
    elif rect_center in get_top_position():
        direction = "up"
        return direction
    elif rect_center in get_bottom_position():
        direction = "down"
        return direction

    elif rect_center in get_start_position():
        direction = random.choice(("right","up","down"))
        return direction





def get_horizontal_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 11:
                for m in range(1,33):
                    for n in range(1,33):
                        items.append(((j*32)+2+m,(i*32)+2+n))
    return items

def get_vertical_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 12:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items




def get_intersection_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 13:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items



def get_lefttop_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 21:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_leftbottom_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 23:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_righttop_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 22:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_rightbottom_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 24:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items



def get_lefttopright_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 31:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_leftbottomright_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 32:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_topleftbottom_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 33:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_toprightbottom_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 34:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items



def get_left_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 43:
                items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_right_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 44:
                items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_top_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 41:
                items.append(((j*32)+2+16,(i*32)+2+16))
    return items

def get_bottom_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 42:
                items.append(((j*32)+2+16,(i*32)+2+16))
    return items



def get_start_position():
    items = []
    for i, row in enumerate(environment()):
        for j, item in enumerate(row):
            if item == 54:
                        items.append(((j*32)+2+16,(i*32)+2+16))
    return items


